package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Periphery {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Periphery() {
    }
    
    public ArrayList<Song> getPeripherySongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Reptile", "Periphery");             //Create a song
         Song track2 = new Song("It's ONly Smiles", "Periphery");
         Song track3 = new Song("Remain Indoors", "Periphery");
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Beatles
         this.albumTracks.add(track2);
         this.albumTracks.add(track3);
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
